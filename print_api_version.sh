#!/bin/sh

#Copyright 2024 Giuseppe Manzoni
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


HelpExit() {
  echo "This script prints the api version of an android.jar." >&2
  echo "It needs two parameters:" >&2
  echo " 1. the path of the android.jar." >&2
  echo " 2. if to print the 'lowest' or 'highest' possible version in case the script can't tell exactly which version." >&2
  exit 1
}

## parameters

# the path to the jar file.
jar="$1"
if [ ! -e "$jar" ] ; then
  HelpExit
fi

# which version to print if this script can't tell.
if [ "$2" = "lowest" ] ; then
  versionLowElseHigh=true  # print the lowest possible version.
elif [ "$2" = "highest" ] ; then
  versionLowElseHigh=false  # print the highest possible version.
else
  HelpExit
fi


## script

# getting the list of files
list=$(mktemp) || exit 1
trap 'rm -rf "$list"; exit' ERR EXIT HUP INT TERM
unzip -l "$jar" > $list

# print the version
if ! cat "$list" | grep -q ' android/' ; then
  echo '0'  # Fake android.jar. For example the one used by aapt
elif ! cat "$list" | grep -q ' android/appwidget/' ; then
  # 1 or 2. TODO: Couldn't find a diff file between the two.
  if $versionLowElseHigh ; then
    echo '1'
  else
    echo '2'
  fi
elif ! cat "$list" | grep -q ' android/accessibilityservice/' ; then
  echo '3'
elif ! cat "$list" | grep -q ' android/accounts/' ; then
  echo '4'
elif ! cat "$list" | grep -q ' android/service/wallpaper/' ; then
  # No new classes I could find
  if $versionLowElseHigh ; then
    echo '5'
  else
    echo '6'
  fi
elif ! cat "$list" | grep -q ' android/app/admin/' ; then
  echo '7'
elif ! cat "$list" | grep -q ' android/nfc/' ; then
  echo '8'
elif ! cat "$list" | grep -q ' android/nfc/tech/' ; then
  echo '9'

elif ! cat "$list" | grep -q ' android/animation/' ; then
  echo '10'
elif ! cat "$list" | grep -q ' android/hardware/usb/' ; then
  echo '11'
elif ! cat "$list" | grep -q ' android/app/Fragment.SavedState' ; then
  echo '12'
elif ! cat "$list" | grep -q ' android/media/effect/' ; then
  echo '13'
elif ! cat "$list" | grep -q ' android/os/TransactionTooLargeException' ; then
  echo '14'
elif ! cat "$list" | grep -q ' android/annotation/' ; then
  echo '15'
elif ! cat "$list" | grep -q ' android/hardware/display/' ; then
  echo '16'
elif ! cat "$list" | grep -q ' android/service/notification/' ; then
  echo '17'
elif ! cat "$list" | grep -q ' android/print/' ; then
  echo '18'
elif ! cat "$list" | grep -q ' android/app/RemoteInput' ; then
  echo '19'

elif ! cat "$list" | grep -q ' android/app/job/' ; then
  echo '20'
elif ! cat "$list" | grep -q ' android/service/carrier/' ; then
  echo '21'
elif ! cat "$list" | grep -q ' android/app/assist/' ; then
  echo '22'
elif ! cat "$list" | grep -q ' android/os/health/' ; then
  echo '23'
elif ! cat "$list" | grep -q ' android/content/pm/ShortcutManager' ; then
  echo '24'
elif ! cat "$list" | grep -q ' android/companion/' ; then
  echo '25'
elif ! cat "$list" | grep -q ' android/app/WallpaperColors' ; then
  echo '26'
elif ! cat "$list" | grep -q ' android/app/slice/' ; then
  echo '27'
elif ! cat "$list" | grep -q ' android/app/role/' ; then
  echo '28'
elif ! cat "$list" | grep -q ' android/app/blob/' ; then
  echo '29'

elif ! cat "$list" | grep -q ' android/app/people/' ; then
  echo '30'
elif ! cat "$list" | grep -q ' android/service/voice/VisibleActivityInfo' ; then
  echo '31'
elif ! cat "$list" | grep -q ' android/os/ext/' ; then
  echo '32'
elif ! cat "$list" | grep -q ' android/credentials/' ; then
  echo '33'
else
  echo '34' # Currently the newest supported
fi

