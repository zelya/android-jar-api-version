
# What it does

This script prints the api version supported by an android.jar.

# How it does it

It checks the differences in the packages or classes contained in the android.jar. One check per api version.

# How to use it

To execute it, it needs two parameters:

 1. the path of the android.jar.

 2. either 'lowest' or 'highest', in case the script can't tell the exact version. This happens in two cases: versions 1-2 and versions 5-6.

